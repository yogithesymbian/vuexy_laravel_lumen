/*=========================================================================================
  File Name: moduleCalendarActions.js
  Description: Calendar Module Actions
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

import axios from "@/axios.js";

export default {
    addItem({ commit }, param) {
        return new Promise((resolve, reject) => {
            axios
                .post(param[0].uri, param[0].item)
                .then(response => {
                    commit(
                        "ADD_ITEM",
                        Object.assign(response.data.data.input_brg_masuk, { id: response.data.data.input_brg_masuk.id })
                    );
                    resolve(response);
                })
                .catch(error => {
                    reject(error);
                });
        });
    },
    fetchDataListItems({ commit }, param) {
        return new Promise((resolve, reject) => {
            axios
                .get(param[0].uri)
                .then(response => {
                    commit("SET_ITEM", response.data.data.sh_brg_masuk);
                    resolve(response);
                })
                .catch(error => {
                    reject(error);
                });
        });
    },
    fetchDataListItemsKodeBarang({ commit }, param) {
        return new Promise((resolve, reject) => {
            axios
                .get(param[0].uri)
                .then(response => {
                    commit("SET_ITEM_LIST_1", response.data.data.sh_barang);
                    resolve(response);
                })
                .catch(error => {
                    reject(error);
                });
        });
    },
    // fetchEventLabels({ commit }) {
    //   return new Promise((resolve, reject) => {
    //     axios.get("/api/apps/calendar/labels")
    //       .then((response) => {
    //         commit('SET_LABELS', response.data)
    //         resolve(response)
    //       })
    //       .catch((error) => { reject(error) })
    //   })
    // },
    updateItem({ commit }, param) {
        return new Promise((resolve, reject) => {
            axios
                .put(param[0].uri, param[0].item)
                .then(response => {
                    console.log("update", response.data.data.update_brg_masuk)
                    commit("UPDATE_ITEM", response.data.data.update_brg_masuk);
                    resolve(response);
                })
                .catch(error => {
                    reject(error);
                });
        });
    },
    removeItem({ commit }, param) {
        return new Promise((resolve, reject) => {
            axios
                .delete(param[0].uri, { data: { id: param[0].id } })
                .then(response => {
                    commit("REMOVE_ITEM", param[0].id);
                    resolve(response);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }
    // eventDragged({ commit }, payload) {
    //   return new Promise((resolve, reject) => {
    //     axios.post(`/api/apps/calendar/event/dragged/${payload.event.id}`, {payload: payload})
    //       .then((response) => {

    //         // Convert Date String to Date Object
    //         let event = response.data
    //         event.startDate = new Date(event.startDate)
    //         event.endDate = new Date(event.endDate)

    //         commit('UPDATE_EVENT', event)
    //         resolve(response)
    //       })
    //       .catch((error) => { reject(error) })
    //   })
    // },
};
