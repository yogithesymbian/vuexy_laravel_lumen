/*=========================================================================================
  File Name: sidebarItems.js
  Description: Sidebar Items list. Add / Remove menu items from here.
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


export default [
  {
    url: null,
    name: "Dashboards",
    tag: "2",
    tagColor: "warning",
    icon: "HomeIcon",
    i18n: "Dashboards",
    submenu: [
        {
          url: "/home",
          name: "Home",
          slug: "home",
          i18n: "home"
        },
        {
          url: "/page2",
          name: "Page 2",
          slug: "page2",
          i18n: "page2"
        }
    ]
},
{
  header: "Apps",
  icon: "PackageIcon",
  i18n: "Apps",
  items: [
      {
        url: "/apps/chat",
        name: "Chat",
        slug: "chat",
        icon: "MessageSquareIcon",
        i18n: "Chat"
      },
      {
          url: "/apps/email",
          name: "Email",
          slug: "email",
          icon: "MailIcon",
          i18n: "Email"
      }

  ]
}
]
